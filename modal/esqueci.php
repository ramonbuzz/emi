<h1 class="t-center">Recuperação</h1>
<p class="t-center">Digite seu email no campo abaixo que lhe enviaremos uma mensagem com seu usuário e senha</p>
<form class="form">
    <input type="text" placeholder="Email:" class="t-azul">
    <input type="submit" value="Recuperar usuário e senha" class="btn-p">
    <a href="modal/login.php" rel="modal:open" class="btn-s btn-full">Voltar para o login</a>
</form>