<h1 class="t-center">Cadastro</h1>
<p class="t-center">Após preencher o formulário, enviaremos um email para confirmar o seu cadastro. <strong>Todos os campos são obrigatórios</strong></p>
<form class="form">
    <input type="text" placeholder="Nome completo:" class="t-azul">
    <input type="text" placeholder="Email:" class="t-azul">
    <input type="text" placeholder="Telefone com DDD:" class="t-azul">
    <input type="text" placeholder="Usuário:" class="t-azul">
    <input type="password" placeholder="Senha:" class="t-azul">
    <input type="submit" value="Cadastrar" class="btn-p">
    <a href="modal/login.php" rel="modal:open" class="btn-s btn-full">Voltar para o login</a>
</form>