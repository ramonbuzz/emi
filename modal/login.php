<h1 class="t-center">Faça seu login</h1>
<form class="form">
    <input type="text" placeholder="Usuário:" class="t-azul">
    <input type="password" placeholder="Senha:" class="t-azul">

    <!-- esse botao eu botei so pro cliente ver como vai funcionar a pagina de compras -->
    <a href="modal/compras.php" rel="modal:open" class="btn-p btn-full">Entrar</a>

    <!-- apaga o de cima e usa esse aqui que tá comentado embaixo -->
    <!-- <input type="submit" value="Entrar" class="btn-p"> -->

    <a href="modal/cadastro.php" rel="modal:open" class="btn-s btn-full">Não tem cadastro?</a>
    <p class="col-100 t-center"><a href="modal/esqueci.php" rel="modal:open" class="t-vermelho">Esqueci o usuário ou a senha!</a></p>
</form>