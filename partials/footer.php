<footer class="t-branco" id="contato">

<div class="grid flex">

    <article class="col-15">
        <img src="assets/img/logo2.png">
    </article>

    <article class="col-45">
        <h1 class="t-amarelo">Fale Conosco</h1>
        <h2>Entre em contato através do nosso endereço, telefone ou preenchendo o formulário ao lado:</h2>
        <h3 class="t-amarelo">Endereço</h3>
        <p>Rua Fulano de Ciclano, N. 686<br>Centro - Fortaleza/CE</p>
        <h3 class="t-amarelo">Telefones</h3>
        <p>(85) 98811.2233 <br>(85) 98811.2233</p>
    </article>

    <article class="col-40">
        <form action="" class="form">
            <input type="text" placeholder="Nome:" class="t-amarelo">
            <input type="text" placeholder="Email:" class="t-amarelo">
            <input type="text" placeholder="Assunto:" class="t-amarelo">
            <input type="text" placeholder="Mensagem:" class="t-amarelo">
            <input type="submit" value="Enviar Mensagem" class="btn-p">
        </form>
    </article>

</div>

</footer>

<p class="creditos t-center t-branco">2020 - Ensino Médico Integrado. Todos os direitos reservados</p>

<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/modal.js"></script>
<script type="text/javascript" src="assets/js/funcoes.js"></script>

</body>

</html>