<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700;900&family=Roboto+Slab:wght@400;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/estilo.css">
    <title>EMI - Ensino Médico Integrado</title>
</head>

<body id="inicio">

    <header>

        <nav class="grid flex v-center h-justify">

            <div class="col-10">
                <a href="#inicio"><img src="assets/img/logo.png" class="logo"></a>
            </div>

            <label for="hamburger">&#9776;</label>
            <input type="checkbox" id="hamburger" />

            <ul class="col-60 flex v-center h-justify">
                <li><a href="index.php">Página Inicial</a></li>
                <li><a href="cursos.php">Cursos</a></li>
                <li><a href="materiais.php">Materiais</a></li>
                <li><a href="#contato">Contato</a></li>
                <li>
                    <form action="" class="pesquisa flex v-center">
                        <input type="text" placeholder="Pesquisar">
                        <button type="submit"><img src="assets/img/lupa.png"></button>
                    </form>
                </li>
                <li><a href="modal/login.php" rel="modal:open"><img src="assets/img/login.png"></a></li>
            </ul>

        </nav>

    </header>