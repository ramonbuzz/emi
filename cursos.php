<?php include('partials/header.php'); ?>

<section>

    <div class="grid flex">

        <article class="col-60">
            <h1>Cursos online</h1>
        </article>

        <article class="col-40">
            <form action="" class="form">
                <select name="" id="">
                    <option value="">Filtrar por área</option>
                    <option value="">Ultrassonografia</option>
                    <option value="">Ressonância</option>
                </select>
            </form>
        </article>

    </div>

    <div class="grid flex lista-cartao">

        <article class="col-100">
            <h2 class="t-vermelho">Ultrassonografia</h2>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

    </div>

    <div class="grid flex lista-cartao">

        <article class="col-100">
            <h2 class="t-vermelho">Ressonância</h2>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

    </div>

</section>

<section>

    <div class="grid flex">

        <article class="col-60">
            <h1>Cursos presenciais</h1>
        </article>

        <article class="col-40">
            <form action="" class="form">
                <select name="" id="">
                    <option value="">Filtrar por área</option>
                    <option value="">Ultrassonografia</option>
                    <option value="">Ressonância</option>
                </select>
            </form>
        </article>

    </div>

    <div class="grid flex lista-cartao">

        <article class="col-100">
            <h2 class="t-vermelho">Ultrassonografia</h2>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

    </div>

    <div class="grid flex lista-cartao">

        <article class="col-100">
            <h2 class="t-vermelho">Ressonância</h2>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

    </div>

</section>

<?php include('partials/footer.php'); ?>