<?php include('partials/header.php'); ?>

<section>

    <div class="grid flex v-bottom">

        <article class="col-100">
            <h1>Ultrassonografia dos membros superiores</h1>
        </article>

        <article class="col-66 flex h-justify v-center t-center detalhes">

            <figure>
                <img src="assets/img/bg-emi2.jpg">
            </figure>
            <a href="#" class="btn-t">Comprar esse curso!</a>
            <h2 class="t-vermelho">R$ 2.000,00</h2>

        </article>

        <article class="col-33 avatar">

            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/avatar.jpg">
                </figure>
                <h3 class="t-vermelho">Quem ministra?</h3>
                <h2>Dr. César Alves</h2>
                <a href="modal/curriculo.php" rel="modal:open" class="btn-s btn-full">Ver currículo</a>
            </div>

        </article>

    </div>

</section>

<div class="grid flex">

    <article class="col-66">

        <ul class="lista">
            <li>
                <h3 class="t-vermelho">Descrição do curso</h3>
            </li>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec placerat ac purus sed pretium. Duis
                placerat sollicitudin neque sed scelerisque. Suspendisse pharetra turpis viverra, ultrices enim eget,
                porta tellus. Cras vehicula eros diam, a sagittis felis fringilla eget. Vestibulum elementum ultrices
                molestie. Donec id ornare ligula. Sed venenatis fringilla mollis. Mauris et aliquet sapien. Integer
                tincidunt eget orci et porttitor. Suspendisse tincidunt erat lectus, sed pharetra magna pulvinar non.
                Nulla pulvinar, nisl ut placerat ullamcorper, purus lacus consequat arcu, tempor finibus nibh neque nec
                lectus. Nunc cursus ante nec aliquet pulvinar. Duis et diam ultricies, ultrices ante non, laoreet nisl.
                Vestibulum condimentum pellentesque elit. Aenean turpis mauris, cursus a tortor eu, lobortis suscipit.
            </li>
        </ul>

        <ul class="lista">
            <li>
                <h3 class="t-vermelho">Conteúdo programático</h3>
            </li>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec placerat ac purus sed pretium. Duis
                placerat sollicitudin neque sed scelerisque. Suspendisse pharetra turpis viverra, ultrices enim eget,
                porta tellus. Cras vehicula eros diam, a sagittis felis fringilla eget. Vestibulum elementum ultrices
                molestie. Donec id ornare ligula. Sed venenatis fringilla mollis. Mauris et aliquet sapien.</li>
        </ul>

    </article>

    <article class="col-33">

        <ul class="lista">
            <li>
                <h3 class="t-vermelho">Público alvo</h3>
            </li>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec placerat ac purus sed pretium. Duis
                placerat sollicitudin neque sed scelerisque. Suspendisse pharetra turpis viverra, ultrices enim eget,
                porta tellus. Cras vehicula eros diam, a sagittis felis fringilla eget. Vestibulum elementum ultrices
                molestie. Donec id ornare ligula. Sed venenatis fringilla.</li>
        </ul>

        <ul class="lista">
            <li>
                <h3 class="t-vermelho">Carga horária</h3>
            </li>
            <li>400 h/a</li>
        </ul>

    </article>

</div>

<section>

    <div class="grid flex">

        <article class="col-100">
            <h1>Mais cursos</h1>
        </article>

    </div>

    <div class="grid flex lista-cartao">

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

        <article class="col-33">
            <div class="cartao t-center">
                <figure>
                    <img src="assets/img/bg-emi.jpg">
                </figure>
                <h2>Ultrassonografia dos Membros Superiores</h2>
                <a href="cursos-interna.php" class="btn-p">Mais Informações</a>
            </div>
        </article>

    </div>

</section>

<?php include('partials/footer.php'); ?>