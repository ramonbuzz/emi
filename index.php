<?php include('partials/header.php'); ?>
    
    <section class="banner">

        <div class="grid flex">

            <article class="col-40">
                <h1 class="t-branco">Somos o Ensino Médico Integrado</h1>
                <h2 class="t-branco">E temos os cursos com melhores custo-benefícios do mercado</h2>
                <a href="#quemsomos" class="btn-p">Clique aqui e saiba mais</a>
            </article>

        </div>

    </section>

    <section>

        <div class="grid flex">

            <article class="col-33">
                <div class="cartao t-center">
                    <img src="assets/img/icon1.png" class="icone">
                    <h2>Estude as aulas quantas vezes quiser</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem nulla fuga accusantium ducimus
                        sunt! Amet eius deleniti ut aspernatur ab.</p>
                </div>
            </article>

            <article class="col-33">
                <div class="cartao t-center">
                    <img src="assets/img/icon2.png" class="icone">
                    <h2>Atividades e testes complementares</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem nulla fuga accusantium ducimus
                        sunt! Amet eius deleniti ut aspernatur ab.</p>
                </div>
            </article>

            <article class="col-33">
                <div class="cartao t-center">
                    <img src="assets/img/icon3.png" class="icone">
                    <h2>Materiais de diversas áreas da saúde</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem nulla fuga accusantium ducimus
                        sunt! Amet eius deleniti ut aspernatur ab.</p>
                </div>
            </article>

        </div>

    </section>

    <section id="quemsomos">

        <div class="grid flex">

            <article class="col-70">
                <h1>Ensino Médico Integrado</h1>
                <h2 class="t-vermelho">Mais confiança e credibilidade</h2>
                <div class="cartao">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Non qui illum optio culpa asperiores.
                        Autem illum dolore delectus distinctio dolorum suscipit cupiditate eveniet, incidunt vero fugit
                        vitae corrupti quia a illo aliquid quod rem dicta totam tempora? Velit adipisci dolor ratione
                        recusandae doloribus autem earum delectus quas vel. Architecto, natus!</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Non qui illum optio culpa asperiores.
                        Autem illum dolore delectus distinctio dolorum suscipit cupiditate eveniet, incidunt vero fugit
                        vitae corrupti quia a illo aliquid quod rem dicta totam tempora? Velit adipisci dolor ratione
                        recusandae doloribus autem earum delectus quas vel. Architecto, natus!</p>
                    <a href="cursos.php" class="btn-t">Conheça os nossos cursos</a>
                </div>
            </article>

            <article class="col-30 bg-emi">
                <figure><img src="assets/img/bg-emi.jpg"></figure>
            </article>

        </div>

    </section>

<?php include('partials/footer.php'); ?>